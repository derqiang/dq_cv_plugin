import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';

import 'dq_cv_plugin_platform_interface.dart';

/// An implementation of [DqCvPluginPlatform] that uses method channels.
class MethodChannelDqCvPlugin extends DqCvPluginPlatform {
  /// The method channel used to interact with the native platform.
  @visibleForTesting
  final methodChannel = const MethodChannel('dq_cv_plugin');

  @override
  Future<String?> getPlatformVersion() async {
    final version = await methodChannel.invokeMethod<String>('getPlatformVersion');
    return version;
  }
}
