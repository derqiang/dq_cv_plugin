
import 'dq_cv_plugin_platform_interface.dart';

class DqCvPlugin {
  Future<String?> getPlatformVersion() {
    return DqCvPluginPlatform.instance.getPlatformVersion();
  }
}
