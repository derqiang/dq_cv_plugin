import 'package:plugin_platform_interface/plugin_platform_interface.dart';

import 'dq_cv_plugin_method_channel.dart';

abstract class DqCvPluginPlatform extends PlatformInterface {
  /// Constructs a DqCvPluginPlatform.
  DqCvPluginPlatform() : super(token: _token);

  static final Object _token = Object();

  static DqCvPluginPlatform _instance = MethodChannelDqCvPlugin();

  /// The default instance of [DqCvPluginPlatform] to use.
  ///
  /// Defaults to [MethodChannelDqCvPlugin].
  static DqCvPluginPlatform get instance => _instance;

  /// Platform-specific implementations should set this with their own
  /// platform-specific class that extends [DqCvPluginPlatform] when
  /// they register themselves.
  static set instance(DqCvPluginPlatform instance) {
    PlatformInterface.verifyToken(instance, _token);
    _instance = instance;
  }

  Future<String?> getPlatformVersion() {
    throw UnimplementedError('platformVersion() has not been implemented.');
  }
}
