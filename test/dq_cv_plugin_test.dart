import 'package:flutter_test/flutter_test.dart';
import 'package:dq_cv_plugin/dq_cv_plugin.dart';
import 'package:dq_cv_plugin/dq_cv_plugin_platform_interface.dart';
import 'package:dq_cv_plugin/dq_cv_plugin_method_channel.dart';
import 'package:plugin_platform_interface/plugin_platform_interface.dart';

class MockDqCvPluginPlatform
    with MockPlatformInterfaceMixin
    implements DqCvPluginPlatform {

  @override
  Future<String?> getPlatformVersion() => Future.value('42');
}

void main() {
  final DqCvPluginPlatform initialPlatform = DqCvPluginPlatform.instance;

  test('$MethodChannelDqCvPlugin is the default instance', () {
    expect(initialPlatform, isInstanceOf<MethodChannelDqCvPlugin>());
  });

  test('getPlatformVersion', () async {
    DqCvPlugin dqCvPlugin = DqCvPlugin();
    MockDqCvPluginPlatform fakePlatform = MockDqCvPluginPlatform();
    DqCvPluginPlatform.instance = fakePlatform;

    expect(await dqCvPlugin.getPlatformVersion(), '42');
  });
}
