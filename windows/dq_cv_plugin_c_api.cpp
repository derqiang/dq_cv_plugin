#include "include/dq_cv_plugin/dq_cv_plugin_c_api.h"

#include <flutter/plugin_registrar_windows.h>

#include "dq_cv_plugin.h"

void DqCvPluginCApiRegisterWithRegistrar(
    FlutterDesktopPluginRegistrarRef registrar) {
  dq_cv_plugin::DqCvPlugin::RegisterWithRegistrar(
      flutter::PluginRegistrarManager::GetInstance()
          ->GetRegistrar<flutter::PluginRegistrarWindows>(registrar));
}
